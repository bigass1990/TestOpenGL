# TestOpenGL

#### 介绍
练习使用https://learnopengl-cn.github.io/教程。

#### 软件架构
软件架构说明


#### 编译教程

/* ---2019.4.18 新版使用当前解决方案下的glfw，不再需要设置环境变量--
1. 下载代码后，在本机添加环境变量 GLFW_DIR ，值为本机上的 \TestOpenGL\CommonLibrary\glfw-3.2.1 目录，例如 E:\Projects\TestOpenGL\CommonLibrary\glfw-3.2.1  。
打开cmake图形客户端，Where is the source code: 选择环境变量指定的目录；Where to build the binaries: 环境变量指定目录下新建一个目录build，选择该目录。
然后点击 Configure，选择要编译的环境。之后点击 Generate，会生成相应的编译环境，cmake的命令行会提示 ....done.
2. 选择上面build下的GLFW.sln打开，默认编译x86的debug版本。
3. 打开项目根目录TestOpenGL下的TestOpenGL.sln文件，编译。
---------------------------------------------------------*/
2019.4.18 -- 直接打开解决方案根目录下的TestOpenGL.sln，可以直接编译通过。

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)