#pragma once

namespace tgl
{
	class GLFWInfo
	{
	public:
		~GLFWInfo();

		static GLFWInfo* GetInstance();

	public:

		// 初始化GLFW库。
		bool InitGLFW();

		// 释放GLFW库资源
		void ReleaseGLFW();

	public:
		int MajorVersion; // 主版本号
		int MinorVersion; // 此版本号


	protected:


		GLFWInfo();
		static GLFWInfo* m_pInfo;
	};



}

