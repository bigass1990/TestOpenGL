#include "pch.h"
#include "Log.h"

#include "spdlog\sinks\rotating_file_sink.h"

namespace tgl
{

	CLog* CLog::s_pLog = nullptr;

	CLog::CLog()
	{
		m_loggerPtr = spdlog::rotating_logger_mt("TestOpenGL-log", "./logs/rotating.txt", 1048576 * 5, 2);
	}

	CLog* CLog::GetInstance()
	{
		if (s_pLog == nullptr)
		{
			s_pLog = new CLog();
		}
		return s_pLog;
	}

	std::shared_ptr<spdlog::logger> CLog::GetLog()
	{
		return GetInstance()->m_loggerPtr;
	}

}

