#pragma once

namespace tgl
{

	/* 渲染
	 * 顶点数组对象：Vertex Arrray Object，VAO
	 * 顶点缓冲对象：Vertex Buffer Object，VBO
	 * 索引缓冲对象：Element Buffer Object，EBO或Index Buffer Object，IBO
	 * */

	class Window;

	class CRander
	{
	public:
		CRander(Window *pWnd);
		virtual ~CRander();

	public:

		bool Init();
		void Update();
		bool Rander();

	protected:
		bool TestShader_init();

	protected:
		bool test();

		bool TestShader();
	protected:

		Window *m_pWnd;

		GLuint m_vfPram;
	};

}