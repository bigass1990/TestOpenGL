#pragma once
#include <string>

namespace tgl
{
	using namespace std;

	namespace GLUtils
	{
		// 从文件中读取着色器代码
		bool ReadShaderSource(const char* filePath, string& content);

		/// <summary>
		/// 检查Opengl错误。
		/// </summary>
		/// <param name="errors">返回错误代码</param>
		/// <returns>如果有错误，返回false。否则，返回true。</returns>
		bool CheckOpenGLError(std::vector<int>& errors = std::vector<int>());
	}
}