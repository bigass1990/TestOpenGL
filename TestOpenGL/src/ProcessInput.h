#pragma once

namespace tgl
{

	class Window;

	// 窗口输入处理类
	class CProcessInput
	{
	public:
		CProcessInput(Window* pWindow);

	public:
		bool Init();

		bool Process();

	protected:
		Window *m_pWindow;
	};

}