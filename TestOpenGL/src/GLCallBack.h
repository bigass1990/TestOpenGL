#pragma once
#include <glfw3.h>

namespace tgl
{
	// 窗口的回调函数。它会在每次窗口大小被调整的时候被调用。
	void framebuffer_size_callback(GLFWwindow* window, int width, int height);

	// glfw错误回调函数
	void error_callback(int error, const char* description);

}
