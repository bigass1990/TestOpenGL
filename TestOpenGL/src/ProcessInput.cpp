#include "pch.h"
#include "ProcessInput.h"
#include "Window.h"

namespace tgl
{


	CProcessInput::CProcessInput(Window* pWindow)
		:m_pWindow(pWindow)
	{

	}

	bool CProcessInput::Init()
	{
		return false;
	}

	bool CProcessInput::Process()
	{
		if (glfwGetKey(m_pWindow->GLFWWindow(), GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(m_pWindow->GLFWWindow(), true);

		return true;
	}


}