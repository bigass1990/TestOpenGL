#pragma once


/// <summary>
/// 着色器类型
/// </summary>
enum class ShaderType
{
	eVertex = GL_VERTEX_SHADER,
	eFagment = GL_FRAGMENT_SHADER,
};
DelareEnumToStr(ShaderType)


struct ShaderData 
{
	ShaderData();
	ShaderData(ShaderType type);
	ShaderData(const std::string& source, ShaderType type);
	ShaderData(std::string&& source, ShaderType type);

	std::string m_source;
	ShaderType m_type;
};

typedef std::vector<ShaderData> ShaderDatas;

/// <summary>
/// 编译着色器
/// </summary>
class ShaderCompile
{
public:
	ShaderCompile(const ShaderData& sData, bool hasLog = true);

	bool Compile();
	GLuint ID() const;

public:

	/// 根据着色器句柄，获取着色器类型
	static ShaderType GetType(GLuint shaderHandle);

	/// 返回着色器是否被删除。
	static bool IsDeleted(GLuint shaderHandle);

	/// 返回是否编译成功。
	static bool IsCompiled(GLuint shaderHandle);

	/// 返回日志长度。
	static int GetLogLength(GLuint shaderHandle);

	/// 获取日志。
	static bool GetLogInfo(GLuint shaderHandle, std::string& logInfo);

	/// 返回源码长度。
	static int GetSourceLength(GLuint shaderHandle);

protected:
	void Init();

protected:
	GLuint m_uID;
	ShaderData m_shaderData;
	bool m_hasLog;
};

/// <summary>
/// 链接着色器
/// </summary>
class ShaderProgram
{
public:
	ShaderProgram(GLuint shader, bool hasLog = true);
	ShaderProgram(const std::vector<GLuint>& vecShader, bool hasLog = true);

	bool Link();
	GLuint ProgramHandle() const;

public:

	// 返回是否已删除。
	static bool IsDeleted(GLuint programHandle);

	/// 返回是否编译成功。
	static bool IsLinked(GLuint programHandle);

	/// 返回日志长度。
	static int GetLogLength(GLuint programHandle);

	// 获取日志。
	static int GetLogInfo(GLuint programHandle, std::string& logInfo);

	// 返回附加到程序的着色器数量。
	static int GetAttechedShaderSize(GLuint programHandle);

	// 返回活动属性的最大长度。
	static int GetActiveAttirbuteMaxLength(GLuint programHandle);

	// 返回活动属性的数量。
	static int GetActiveAttirbuteSize(GLuint programHandle);

	// 返回活动状态的统一变量的数量。
	static int GetAcviteUniformSize(GLuint programHandle);

	// 返回活动状态的统一变量的名称最大长度。
	static int GetAcviteUniformMaxLength(GLuint programHandle);

protected:
	std::vector<GLuint> m_vecShader;
	GLuint m_program;

	bool m_hasLog;
};

class Shader
{
public:

	
	static GLuint CreateProgram(ShaderData data);

	static GLuint CreateProgram(ShaderDatas datas);

};

