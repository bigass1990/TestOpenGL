#pragma once
#include <string>

#ifdef UNICODE
#define String std::wstring
#define TEXT(str) L##str
#else
#define String std::string
#define TEXT(str) str
#endif
namespace tgl
{
	using namespace std;

	class CApplicationService
	{
	public:
		~CApplicationService();

		static CApplicationService* GetInstance();

	public:
		// 返回安装目录
		// 例如： "C:\\Program Files\\TestOpengl\\"
		String GetInstallPath() const;

		// 返回用户目录
		// 例如： "C:\\Users\\xxx\\AppData\\Local\\"
		String GetUserPath() const;

	protected:

	private:
		CApplicationService();

		static CApplicationService* m_pService;
	};

#define ApplicationService (CApplicationService::GetInstance())
}