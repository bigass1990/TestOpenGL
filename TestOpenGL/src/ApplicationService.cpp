#include "pch.h"
#include "ApplicationService.h"

namespace tgl
{
	CApplicationService* CApplicationService::m_pService = nullptr;

	CApplicationService::~CApplicationService()
	{
		if (m_pService != nullptr)
		{
			delete m_pService;
			m_pService = nullptr;
		}
	}

	CApplicationService* CApplicationService::GetInstance()
	{
		if (m_pService == nullptr)
		{
			m_pService = new CApplicationService();
		}

		return m_pService;
	}

	String CApplicationService::GetInstallPath() const
	{
		String str(TEXT("../Install"));
		return str;
	}

	String CApplicationService::GetUserPath() const
	{
		String str(TEXT("../User"));
		return str;
	}

	CApplicationService::CApplicationService()
	{

	}

}


