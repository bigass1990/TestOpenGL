#pragma once


namespace tgl
{

	class Window
	{
	public:
		Window();
		Window(int width, int height, const std::string& title = "OpenGL");
		~Window();

	public:

		// 返回窗口是否可用
		inline bool IsWindowEnable() const;

		// 返回GLFWWWindow指针
		const GLFWwindow* GLFWWindow() const;
		GLFWwindow* GLFWWindow();

		// 显示窗口
		bool Show();

	public:

		// 是否要关闭窗口
		int ShouldClose() const;

	public:

		// 窗口宽
		inline int Width() const;
		inline void SetWidth(int width);

		// 窗口高
		inline int Height() const;
		inline void SetHeight(int height);

		// 窗口标题
		inline std::string Title() const;
		inline void SetTitle(const std::string& title);

	private:
		void Init();

	private:
		int m_nWidth;
		int m_nHeight;
		std::string m_strTitle;

	private:
		GLFWwindow *m_glfwWindow; // 主窗口
		GLFWmonitor *m_glfwMonitor; // 是否为null，是否允许全屏显示
		GLFWwindow *m_glfwShareWnd; // 资源共享

	private:
		static int s_MajorVersion; // 主版本号
		static int s_MinorVersion; // 此版本号

	private:

	};

}

