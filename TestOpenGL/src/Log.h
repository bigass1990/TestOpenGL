#pragma once

#include "spdlog\spdlog.h"

namespace tgl
{

	class CLog
	{
	public:
		static CLog* GetInstance();
		static std::shared_ptr<spdlog::logger> GetLog();
	protected:
		CLog();

	protected:
		static CLog* s_pLog;
		std::shared_ptr<spdlog::logger> m_loggerPtr;
	};



}