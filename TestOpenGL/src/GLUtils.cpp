#include "pch.h"
#include "GLUtils.h"


using namespace tgl;
using namespace std;

bool GLUtils::ReadShaderSource(const char* filePath, string& content)
{
	ifstream fileStream(filePath, ios::in);
	string line = "";

	while (!fileStream.eof())
	{
		getline(fileStream, line);
		content.append(line + "\n");
	}
	fileStream.close();

	return true;
}

bool tgl::GLUtils::CheckOpenGLError(std::vector<int>& errors /*= std::vector<int>()*/)
{
	bool foundError = false;
	int glError = glGetError();
	while (glError != GL_NO_ERROR)
	{
		errors.push_back(glError);
		foundError = true;
		glError = glGetError();
	}

	return foundError;
}
