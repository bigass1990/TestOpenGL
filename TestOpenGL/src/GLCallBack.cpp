#include "pch.h"
#include "GLCallBack.h"
#include "Log.h"


namespace tgl
{

	void framebuffer_size_callback(GLFWwindow* window, int width, int height)
	{
		glViewport(0, 0, width, height);

	}

	void error_callback(int error, const char* description)
	{
		LOG->error("GLFW:%d-%s", error, description);
	}

}