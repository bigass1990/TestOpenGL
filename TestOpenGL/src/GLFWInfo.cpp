#include "pch.h"
#include "GLFWInfo.h"
#include "Log.h"
#include "GLCallBack.h"

namespace tgl
{
	GLFWInfo* GLFWInfo::m_pInfo = NULL;
	GLFWInfo::GLFWInfo()
	{
		MajorVersion = 4;
		MinorVersion = 6;
	}

	GLFWInfo::~GLFWInfo()
	{
		ReleaseGLFW();
	}

	GLFWInfo* GLFWInfo::GetInstance()
	{
		if (m_pInfo == NULL)
		{
			m_pInfo = new GLFWInfo;
		}
		return m_pInfo;
	}

	bool GLFWInfo::InitGLFW()
	{
		glfwSetErrorCallback(error_callback);

		// 初始化GLFW
		/* 它会处理程序输入的命令行参数，并移除其中
		于控制glfw如何操作相关的部分（如设置窗口大小）*/
		int rel = glfwInit();
		if (rel)
		{
			/* glfwWindowHint -- 第一个参数表示选项名称；第二个参数设置这个选项的值。*/
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, MajorVersion); // OpenGL主版本好
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, MinorVersion); // OpenGL次版本号
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // 指定使用核心模式

			LOG->info(StringUtils::Format("major:%d  minor:%d use opengl core Profile", MajorVersion, MinorVersion));
		}
		else
		{
			LOG->error("glfwInit error!");
		}

		return rel == GLFW_TRUE;
	}

	void GLFWInfo::ReleaseGLFW()
	{
		// 释放之前分配的所有资源。
		glfwTerminate();
	}

}