#include "pch.h"
#include "Rander.h"
#include "Window.h"
#include "Log.h"
#include "Shader.h"

namespace tgl
{

	CRander::CRander(Window *pWnd)
		:m_pWnd(pWnd)
	{

	}

	CRander::~CRander()
	{

	}

	bool CRander::Init()
	{
		return TestShader_init();
	}

	void CRander::Update()
	{

	}

	const char* vertexShaderSource = "#version 330 core\n"
		"layout (location = 0) in vec3 aPos;\n"
		"void main()\n"
		"{\n"
		"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
		"}\0";


	// 片段着色器只需要一个输出变量，这个变量是一个4分量向量，表示的是最终输出颜色。
	const char *fragementShaderSource = "#version 330 core\n"
		"out vec4 FragColor;\n"
		"void main()\n"
		"{\n"
		"   FragColor = vec4(0.0f, 0.5f, 1.0f, 1.0f);\n"
		"}\n\0";

	bool CRander::Rander()
	{
		return TestShader();

		// 开始绘图之前，先给OpenGL输入一些顶点数据。
		float vertices[] =
		{
			-0.5f, -0.5f, 0.0f,
			0.5f, -0.5f, 0.0f,
			0.5f, 0.5f, 0.0f
		};

		// 顶点数据定义后，作为输入发送给图形渲染管线的第一个处理阶段：顶点着色器。
		/* 顶点着色器会在GPU上创建内存用于存储我们的顶点数据，
		 * 还要配置OpenGL如何解释这些内存，并指定其如何发送给显卡。
		 * 我们通过顶点缓冲对象（VBO）管理内存，它会在GPU内存（通常称为显存）中储存
		 * 大量顶点。使用缓冲对象的好处是可以一次发送大量数据到显卡上，而不是每个顶点
		 * 发送一次。从cpu发送数据到显卡相对缓慢。但当数据发送到显存后，顶点着色器几乎
		 * 能立即访问顶点，这是非常快的过程。
		 * */

		unsigned int VBO; // 顶点缓冲对象ID。
		unsigned int VAO; // 顶点数组对象。
		glGenBuffers(1, &VBO);
		glGenVertexArrays(1, &VAO);

		glBindVertexArray(VAO);

		/* 顶点缓冲对象的类型是GL_ARRAY_BUFFER。OpenGL允许同时绑定对各缓冲，只要它们
		 * 是不同的缓冲类型。*/
		glBindBuffer(GL_ARRAY_BUFFER, VBO);

		// 绑定之后，任何在GL_ARRAY_BUFFER上的缓冲调用都会用来配置当前绑定的的缓冲。
		// 复制顶点数据到当前顶点缓冲对象的内存。
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		// 顶点着色器

		/* 使用着色器语言GLSL(OpenGL Shading Language)编写顶点着色器，然后编译这个
		 * 着色器，然后就可以在程序中使用它了。*/


		 // 编译着色器

		unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER); // 创建着色器对象
		glShaderSource(vertexShader, 1, &vertexShaderSource, NULL); // 将着色器源码附加到着色器对象上
		glCompileShader(vertexShader);  // 编译。

		int success;
		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			GLchar infolog[512];
			glGetShaderInfoLog(vertexShader, 512, NULL, infolog);
			LOG->error("Error:Shader:Vertex:Compilation_Falied %s", infolog);
		}


		// 片段着色器：是计算像素最后的颜色输出。
		/* 计算机图形中颜色被表示为4个元素的数组：红色、绿色、蓝色和alpha（透明）分量，
		 * 通常缩写为RGBA。当在OpenGL或GLSL中定义一个颜色时，我们把颜色的每个分量的强度
		 * 设置在0.0到1.0之间。这三种颜色分量的不同调配可以生成超过1600万种不同的颜色。
		 * */

		unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER); // 创建着色器对象
		glShaderSource(fragmentShader, 1, &fragementShaderSource, NULL); // 将着色器源码附加到着色器对象上
		glCompileShader(fragmentShader);  // 编译。


		// 把两个着色器对象连接到一个用来渲染的着色器程序（Shader Program）中。
		// 着色器程序对象（Shader Program Object）是多个着色器合并后最终链接完成的版本。
		// 当链接着色器至下一个程序的时候，它会把每个着色器的输出链接到下一个的输入。
		// 当输出和输入不匹配时，会得到一个链接错误。

		unsigned int shaderProgrom = glCreateProgram(); // 创建程序。
		glAttachShader(shaderProgrom, vertexShader); // 将编译好的着色器附加到程序对象上。
		glAttachShader(shaderProgrom, fragmentShader);
		glLinkProgram(shaderProgrom); // 链接。

		// 获取链接结果
		glGetProgramiv(shaderProgrom, GL_LINK_STATUS, &success);
		if (!success)
		{
			char infolog[512];
			glGetProgramInfoLog(shaderProgrom, 512, NULL, infolog);
			LOG->error("Error:Shader:LinkProgram_Falied");
			LOG->error(infolog);
		}

		// 激活程序对象。激活之后，每个着色器调用和渲染都会使用这个程序对象。
		glUseProgram(shaderProgrom);

		// 删除着色器对象。
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		// 到此时，输入顶点数据已经发送给gpu了，并指示了gpu如何在顶点和片段着色器中处理它。
		// 但OpenGL还不知道它该如何解释内存中的顶点数据，以及如何将顶点数据链接到顶点着色器
		// 的属性上。

		glVertexAttribPointer(
			0, // 指定要配置的顶点属性。即在顶点着色器中使用 layout (location = 0) 定义的位置值。
			3, // 指定顶点属性的大小。
			GL_FLOAT, // 指定数据的类型。
			GL_FALSE, // 指点是否希望数据被标准化（Normalize）。如果是，所有数据被映射到0（如果是signed类型则为-1）到1之间。
			3 * sizeof(float), // 步长（Stride）。指定在连续的顶点属性组之间的间隔。
			(void*)0 // 表示位置数据在缓冲中起始位置的偏移量。
		);

		// 启用顶点属性。
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		glDrawArrays(GL_TRIANGLES, 0, 3);
		return true;
	}

	bool CRander::test()
	{
		// 顶点位置
		static const GLfloat positions[] =
		{
		-1.0f, -1.0f, 0.0f, 1.0f,
		1.0f, -1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 0.0f, 1.0f,
		-1.0f, 1.0f, 0.0f, 1.0f,
		};

		// 顶点颜色
		static const GLfloat colors[] =
		{
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		};

		// 缓存对象
		GLuint buffer;

		// 创建新的缓存对象
		//glCreateBuffers();

		return true;
	}

	GLuint createShaderProgram() 
	{
		const char* vshaderSource =
			"#version 430    \n"
			"void main(void) \n"
			"{ gl_Position = vec4(0.0, 0.0, 0.0, 1.0); }";

		const char* fshaderSource =
			"#version 430    \n"
			"out vec4 color; \n"
			"void main(void) \n"
			"{ color = vec4(0.0, 0.0, 1.0, 1.0); }";

		GLuint vShader = glCreateShader(GL_VERTEX_SHADER);
		GLuint fShader = glCreateShader(GL_FRAGMENT_SHADER);
		GLuint vfprogram = glCreateProgram();

		glShaderSource(vShader, 1, &vshaderSource, NULL);
		glShaderSource(fShader, 1, &fshaderSource, NULL);
		glCompileShader(vShader);
		glCompileShader(fShader);

		glAttachShader(vfprogram, vShader);
		glAttachShader(vfprogram, fShader);
		glLinkProgram(vfprogram);

		return vfprogram;
	}

	bool CRander::TestShader_init()
	{
		std::string root = FileUtils::GetRootDir();
		ShaderData fagData(ShaderType::eFagment), vtxData(ShaderType::eVertex);
		FileUtils::ReadFile(root + "glsl\\fragshader.glsl", fagData.m_source);
		FileUtils::ReadFile(root + "glsl\\vertshader.glsl", vtxData.m_source);
		ShaderDatas shaders;
		shaders.push_back(fagData);
		shaders.push_back(vtxData);
		m_vfPram = Shader::CreateProgram(shaders);
		glUseProgram(m_vfPram);

		GLuint vao;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		return true;

		GLfloat vertices[6][2] = {
			{-0.9, -0.9},
			{0.85, -0.9},
			{-0.9, 0.85},
			{0.9, -0.85},
			{0.9, 0.9},
			{-0.85, 0.9},
		};

		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBufferData(vbo, sizeof(vertices), vertices, 0);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);


		return true;
	}

	float inc = 0.01f;
	float x = 0.0f;
	float offsetLoc;

	bool CRander::TestShader()
	{
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		x += inc;
		if (x > 1.0f) inc = -0.01f;
		if (x < -1.0f) inc = 0.01f;

		offsetLoc = glGetUniformLocation(m_vfPram, "offset");
		glProgramUniform1f(m_vfPram, offsetLoc, x);
		
		//glPointSize(40.0f);
		glDrawArrays(GL_TRIANGLES, 0, 3);

		return true;
	}

}