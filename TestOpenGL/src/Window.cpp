#include "pch.h"
#include "Window.h"
#include "GLCallBack.h"
#include "Rander.h"
#include "ProcessInput.h"
#include "Log.h"

namespace tgl
{
	Window::Window()
	{
		m_nWidth = 800;
		m_nHeight = 600;
		m_strTitle = "OpenGL";
		m_glfwMonitor = nullptr;
		m_glfwShareWnd = nullptr;

		Init();
	}

	Window::Window(int width, int height, const std::string& title /*= "OpenGL"*/)
	{
		m_nWidth = width;
		m_nHeight = height;
		m_strTitle = title;
		m_glfwMonitor = nullptr;
		m_glfwShareWnd = nullptr;

		Init();
	}

	Window::~Window()
	{
		if (m_glfwWindow != nullptr)
		{
			glfwDestroyWindow(m_glfwWindow);
		}
	}

	void Window::Init()
	{
		m_glfwWindow = nullptr;

		m_glfwWindow = glfwCreateWindow(m_nHeight, m_nHeight, m_strTitle.c_str(), m_glfwMonitor, m_glfwShareWnd);

		// 注册窗口尺寸改变时的回调函数。
		glfwSetFramebufferSizeCallback(m_glfwWindow, framebuffer_size_callback);
	}

	bool Window::IsWindowEnable() const
	{
		return m_glfwWindow != nullptr;
	}

	const GLFWwindow* Window::GLFWWindow() const
	{
		return m_glfwWindow;
	}

	GLFWwindow* Window::GLFWWindow()
	{
		return m_glfwWindow;
	}

	bool Window::Show()
	{
		if (!IsWindowEnable())
		{
			return false;
		}

		// 通知glfw将上一步创建的window设置为当前线程的主上下文。
		glfwMakeContextCurrent(m_glfwWindow);

		// 设置current context的交换间隔。必须在已有当前context时才能调用。
		glfwSwapInterval(1);

		// GLAD是用来管理OpenGL的函数指针的，所以在调用任何OpenGL的函数之前，
		// 需要初始化GLAD。
		// 我们给GLAD传入了用来加载系统相关的OpenGL函数指针地址的函数。
		// GLFW给我们的是glfwGetProcAddress，它根据我们编译的系统定义了正确的函数。
		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		{
			LOG->error("Failed to initialize GLAD");
			return false;
		}

		// 视口
		// glViewport -- 前两个参数控制窗口左下角；第三、四个参数控制
		// 渲染窗口的宽度和高度（像素）。
		/* OpenGL幕后使用glViewport中定义的位置和宽高进行2D坐标的转换，
		 * 将OpenGL中的位置坐标转换为你的屏幕坐标。
		 * 例如，OpenGL中的坐标(-0.5, 0.5)有可能（最终）被映射为屏幕中
		 * 的坐标(200,450)。注意，处理过的OpenGL坐标范围只为-1到1，
		 * 因此我们事实上将(-1到1)范围内的坐标映射到(0, 800)和(0, 600)。*/
		glViewport(0, 0, m_nWidth, m_nHeight);


		/* 还可以将我们的函数注册到其他很多回调函数中。一般在创建窗口之后，
		 * 渲染循环初始化之前注册这些回调函数。*/

		 // 用户输入处理
		CProcessInput processInput(this);
		processInput.Init();

		// 渲染
		CRander rander(this);
		rander.Init();

		 // 渲染循环（Render Looop）
		while (!ShouldClose())
		{
			// 指定颜色缓冲区清除后填充的颜色。
			// 设置清空屏幕所使用的颜色。这里使用一个自定义的颜色清空屏幕。
			// 当调用glClear函数，清楚颜色缓冲后，整个颜色缓冲都会被填充为
			// glClearColor设置的颜色。
			//glClearColor(0.2f, 0.3f, 0.3f, 1.0f); // 状态设置函数

			// void glClear (GLbitfield mask);
			// 清空屏幕的颜色缓冲，它接受一个缓冲位(Buffer Bit)来指定要清空的缓冲，
			// 可能的缓冲位有GL_COLOR_BUFFER_BIT，GL_DEPTH_BUFFER_BIT
			// 和GL_STENCIL_BUFFER_BIT。
			// GL_COLOR_BUFFER_BIT :引用了包含渲染后像素的颜色缓冲区。
			//glClear(GL_COLOR_BUFFER_BIT); // 状态使用函数

			processInput.Process();

			rander.Update();
			rander.Rander();

			/* 双缓冲(Double Buffer)
			 * 应用程序使用单缓冲绘图时可能会存在图像闪烁的问题。 这是因为生成的图像不是
			 * 一下子被绘制出来的，而是按照从左到右，由上而下逐像素地绘制而成的。最终图像
			 * 不是在瞬间显示给用户，而是通过一步一步生成的，这会导致渲染的结果很不真实。
			 * 为了规避这些问题，我们应用双缓冲渲染窗口应用程序。前缓冲保存着最终输出的图像，
			 * 它会在屏幕上显示；而所有的的渲染指令都会在后缓冲上绘制。当所有的渲染指令
			 * 执行完毕后，我们交换(Swap)前缓冲和后缓冲，这样图像就立即呈显出来，
			 * 之前提到的不真实感就消除了。*/

			 /* glfwSwapBuffers函数会交换颜色缓冲（它是一个储存着GLFW窗口每一个像素
			  * 颜色值的大缓冲），它在这一迭代中被用来绘制，并且将会作为输出显示在屏幕上。*/
			glfwSwapBuffers(m_glfwWindow);

			/* glfwPollEvents函数检查有没有触发什么事件（比如键盘输入、鼠标移动等）、
			 * 更新窗口状态，并调用对应的回调函数（可以通过回调方法手动设置）。*/
			glfwPollEvents();
		}

		return true;
	}

	int Window::ShouldClose() const
	{
		return glfwWindowShouldClose(m_glfwWindow);
	}

	inline int Window::Width() const
	{
		return m_nWidth;
	}

	void Window::SetWidth(int width)
	{
		m_nWidth = width;
	}

	int Window::Height() const
	{
		return m_nHeight;
	}

	void Window::SetHeight(int height)
	{
		m_nHeight = height;
	}

	std::string Window::Title() const
	{
		return m_strTitle;
	}

	void Window::SetTitle(const std::string& title)
	{
		m_strTitle = title;
	}



}