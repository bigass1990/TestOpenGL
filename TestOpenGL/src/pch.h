#pragma once


#include <string>
#include <iostream>
#include <fstream>

#include "glad/glad.h"
#include "glfw3.h"

#include "StringLib.h"
#include "FileLib.h"
#include "MathLib.h"

#include "Log.h"
#include "Shader.h"

#define LOG (tgl::CLog::GetLog())
#define GLFWINFO (tgl::GLFWInfo::GetInstance())