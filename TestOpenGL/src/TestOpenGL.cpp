﻿#include "pch.h"
#include "GLFWInfo.h"
#include "Window.h"
#include "Log.h"

using namespace tgl;

int main()
{
	if (!GLFWINFO->InitGLFW())
	{
		return 0;
	}

	// 创建一个窗口对象。
	/* 还创建了一个于窗口关联的OpenGL设备环境。
	 * 在使用环境前，必须设置它为当前环境。在一个程序中，
	 可以设置多个设备环境，但用户指令只会传递到当前设备环境。*/
	Window *pWindow = new Window(800, 600);
	if (!pWindow->IsWindowEnable())
	{
		return -1;
	}

	pWindow->Show();

	glfwTerminate();
	
	return 0;
}
